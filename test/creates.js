require('should');
const zapier = require('zapier-platform-core');

const App = require('../index');
const appTester = zapier.createAppTester(App);

describe('creates', () => {

  describe('create job create', () => {
    it('should create a new job', (done) => {
      const bundle = {
        inputData: {
          "createdAt": "2018-03-15T10:20:05+08:00",
          "recieverContactNumber": "09353604184",
          // "completedWithin": 1.8e+6,
          // "completedWithin": "03-31-2018,5:30pm",
          "completedWithin": "Today, 6:00pm, 9:00pm, GoogleDIVIDERdelivery-date, preferred-from-time-of-delivery, preferred-to-time-of-delivery, how-did-you-hear-about-us",
          "refNo": "",
          "hubId": "-KokExCJGEbp7lhjrKpa",
          "custFirstName": "Zapier 3/13 2",
          "custLastName": "Test",
          "custEmail": "emmanuel.nipal@radicalsolutionsinc.com",
          "jobDetails": "",
          "remarks": "",
          "locationDetails": "test",
          "landMark": "landmark test",
          "province": "Metro Manila",
          "city": "Mandaluyong",
          "riderId":"",
          "client":"SANDBOX_DEV",
          "apiKey":"e1004c1ec04c20dd92c137a218c5c6f617dff9d3"
        }
      };

      appTester(App.creates.job.operation.perform, bundle)
        .then((result) => {
          console.log('result', result)
          result.should.have.property('succes');
          done();
        })
        .catch(done);
    });
  });
});
