// We recommend writing your creates separate like this and rolling them
// into the App definition at the end.
const moment = require('moment')
module.exports = {
  key: 'job',

  // You'll want to provide some helpful display labels and descriptions
  // for users. Zapier will put them into the UX.
  noun: 'Job',
  display: {
    label: 'Create Job',
    description: 'Creates a new job.'
  },

  // `operation` is where the business logic goes.
  operation: {
    inputFields: [
      // {key: 'name', required: true, type: 'string'},
      // {key: 'directions', required: true, type: 'text', helpText: 'Explain how should one make the recipe, step by step.'},
      // {key: 'authorId', required: true, type: 'integer', label: 'Author ID'},
      // {key: 'style', required: false, type: 'string', helpText: 'Explain what style of cuisine this is.'},
      {key: 'createdAt', required: true, helpText: 'Date in YYYY-DD-MMTHH:mm:ssZ format'},
      {key: 'recieverContactNumber', required: true, label: 'Receiver Contact Number'},
      {key: 'completedWithin', required: true, label: 'Deliver By', helpText: 'Date in YYYY-DD-MM,h:mma format'},
      {key: 'refNo', label: 'Reference Number'},
      {key: 'hubId', required: true, label: 'Hub ID', helpText: 'Contact your provider for info.'},
      {key: 'custFirstName', required: true, label: "Customer's First Name"},
      {key: 'custLastName', required: true, label: "Customer's Last Name"},
      {key: 'custEmail', required: false, label: "Customer's Email"},
      {key: 'jobDetails', label: 'Job Details'},
      {key: 'remarks', label: 'Remarks'},
      {key: 'locationDetails', required: true, label: 'Location Details'},
      {key: 'landMark', label: 'Landmark'},
      {key: 'province', required: true, helpText: 'e.g. Metro Manila'},
      {key: 'city', required: true},
      {key: 'riderId'},
      {key: 'client', label: 'FleetPH Client Code', required: true, helpText: 'Contact your provider for info.'},
      {key: 'apiKey', required: true, helpText: 'Contact your provider for API key.'}
    ],
    perform: (z, bundle) => {

      let getData = bundle.inputData.completedWithin.split('DIVIDER')
      let getValue = getData[0].replace(" ","").split(',')
      let getName = getData[1].replace(" ","").split(',')
      let completedWithin = {}
      for (let key in getValue) {
        completedWithin[getName[key]] = getValue[key]
      }
      let getDate = completedWithin['delivery-date'].includes('Today') ? moment().format('YYYY-MM-DD') 
                  : completedWithin['delivery-date'].includes('Tomorrow') ? moment().add(1, 'day').format('YYYY-MM-DD') 
                  : completedWithin['delivery-date'].includes('SpecificDate') ? moment().format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')

      let formatCompleteWithin = `${moment(getDate).format('YYYY-MM-DD')}T${moment(completedWithin['preferred-from-time-of-delivery'],'h:mm a').format('HH:mm')}`
      let createdAt = moment(bundle.inputData.createdAt).add(8,'hours').format('YYYY-MM-DDTHH:mm')
      let getCompleteWithin = moment(formatCompleteWithin).diff(createdAt)

      const promise = z.request({
        url: `https://us-central1-fleet-ph.cloudfunctions.net/pushJob?client=${bundle.inputData.client}&apiKey=${bundle.inputData.apiKey}`,
        method: 'POST',
        body: JSON.stringify({
          "createdAt": createdAt,
          "recieverContactNumber": bundle.inputData.recieverContactNumber,
          "completedWithin": getCompleteWithin,
          "refNo": bundle.inputData.refNo ? bundle.inputData.refNo : "",
          "hubId": bundle.inputData.hubId,
          "custFirstName": bundle.inputData.custFirstName,
          "custLastName": bundle.inputData.custLastName,
          "custEmail": bundle.inputData.custEmail ? bundle.inputData.custEmail : "",
          "jobDetails": bundle.inputData.jobDetails ? bundle.inputData.jobDetails : "",
          "remarks": bundle.inputData.remarks ? bundle.inputData.remarks : "",
          "locationDetails": bundle.inputData.locationDetails,
          "landMark": bundle.inputData.landMark ? bundle.inputData.landMark : "",
          "province": bundle.inputData.province,
          "city": bundle.inputData.city,
          "riderId":bundle.inputData.riderId ? bundle.inputData.riderId : ""
        }),
        headers: {
          'content-type': 'application/json'
        }
      });
      return promise.then((response) => JSON.parse(response.content));
    },
    
    // In cases where Zapier needs to show an example record to the user, but we are unable to get a live example
    // from the API, Zapier will fallback to this hard-coded sample. It should reflect the data structure of
    // returned records, and have obviously dummy values that we can show to any user.
    sample: {
      "createdAt": "2018-02-26T11:00",
      "recieverContactNumber": "09951264330",
      "completedWithin": 1.8e+6,
      "refNo": 1231,
      "hubId": "-KokExCJGEbp7lhjrKpa",
      "custFirstName": "Zapier",
      "custLastName": "Test",
      "custEmail": "user@fleet.com",
      "jobDetails": "Deliver asap",
      "remarks": "Along corner Calbayog st ",
      "locationDetails": "536 Calbayog St., Brgy. Highway Hills, Mandaluyong City, Philippines 1550",
      "landMark": "Boni Station",
      "province": "Metro Manila",
      "city": "Mandaluyong City",
      "riderId":"",
      "client":"SANDBOX_DEV",
      "apiKey":"919b554f3187150948f5fce588c751d5d30fcc70"
    },

    // If the resource can have fields that are custom on a per-user basis, define a function to fetch the custom
    // field definitions. The result will be used to augment the sample.
    // outputFields: () => { return []; }
    // Alternatively, a static field definition should be provided, to specify labels for the fields
    outputFields: [
      // {key: 'id', label: 'ID'},
      {key: 'createdAt', label: 'Created At'},
      // {key: 'name', label: 'Name'},
      // {key: 'directions', label: 'Directions'},
      // {key: 'authorId', label: 'Author ID'},
      // {key: 'style', label: 'Style'},
      {key: 'recieverContactNumber', label: 'Contact Number'},
      {key: 'completedWithin', label: 'Deliver By'},
      {key: 'refNo', label: 'Reference Number'},
      {key: 'hubId', label: 'hub'},
      {key: 'custFirstName', label: 'First Name'},
      {key: 'custLastName', label: 'Last Name'},
      {key: 'custEmail', label: 'Email'},
      {key: 'jobDetails', label: 'Details'},
      {key: 'remarks', label: 'Remarks'},
      {key: 'locationDetails', label: 'Location Details'},
      {key: 'landMark', label: 'Landmark'},
      {key: 'province', label: 'Province'},
      {key: 'city', label: 'City'},
      {key: 'riderId', label: 'Rider Id'},
      {key: 'client', label: 'FleetPH Client Code'},
      {key: 'apiKey', label: 'Api Key'}
    ]
  }
};
