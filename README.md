# Fleet_Zapier

Integration to E-Comm Plugin (Shopify) with Zapier

Zapier Quick Start:

- npm install
- zapier login

Zapier Deploy

- zapier push

Zapier Test:

- zapier test

Zapier Quick Tutorial: 
https://github.com/zapier/zapier-platform-cli?utm_source=zapier.com&utm_medium=referral&utm_campaign=zapier#quickstart

Zapier Full Tutorial: 
https://zapier.com/developer/documentation/v2/

Zapier Template Apps:
https://github.com/zapier/zapier-platform-cli/wiki/Example-Apps